<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>SanberBook | Form</title>
  </head>

  <body>
    <div>
      <h1>Buat Account Baru!</h1>
    </div>

    <div>
      <h3>Sign Up Form</h3>
      <form action="/welcome" method="POST">
        @csrf
        <label>First name:</label>
        <br />
        <br />
        <input type="text" name="firstname"/>
        <br />
        <br />

        <label>Last name:</label>
        <br />
        <br />
        <input type="text" name="lastname" />
        <br />
        <br />

        <label>Gender:</label>
        <br />
        <br />
        <input type="radio" name="gender" value="male" />Male <br />
        <input type="radio" name="gender" value="female" />Female <br />
        <input type="radio" name="gender" value="other" />other <br />
        <br />

        <label>Nationality:</label>
        <br />
        <br />
        <select name="nationality">
          <option value="indonesian">Indonesian</option>
          <option value="singaporean">Singaporean</option>
          <option value="malaysian">Malaysian</option>
          <option value="germany">Germany</option>
        </select>
        <br />
        <br />

        <label>Language spoken:</label>
        <br />
        <br />
        <input type="checkbox" name="language" value="bahasa indonesia" />Bahasa
        Indonesia <br />
        <input type="checkbox" name="language" value="english" />English <br />
        <input type="checkbox" name="language" value="germany" />Germany <br />
        <input type="checkbox" name="language" value="other" />other <br />
        <br />

        <label>Bio:</label>
        <br />
        <br />
        <textarea name="message" cols="35" rows="10"></textarea>
        <br />

        <input type="submit" value="Sign Up" />
      </form>
    </div>
  </body>
</html>
