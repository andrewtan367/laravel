<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>SanberBook | Welcome</title>
    <style>
      h1, h3, p {
        font-family: Arial, Helvetica, sans-serif;
      }

      p {
        font-size: 1.5rem;
      }
    </style>
  </head>

  <body>
  <h1>SELAMAT DATANG {{$firstname}} {{$lastname}}</h1>
    <h3>
      Terima kasih telah bergabung di SanberBook. Social Media kita bersama!
    </h3>
    <p>Gender: {{$gender}}</p>
    <p>Nationality: {{$nationality}}</p>
    <p>Bio: {{$bio}}</p>
  </body>
</html>
