<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// untuk menampilkan index.blade.php di http://127.0.0.1:8000/
Route::get('/', function () {
    return view('index');
});

// untuk menampilkan form.blade.php di http://127.0.0.1:8000/register
Route::get("/register", "HomeController@form");

// untuk menampilkan welcome.blade.php di http://127.0.0.1:8000/welcome
Route::post("/welcome", "AuthController@login");
