<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    // return welcome.blade.php
    public function login(Request $request) {
        $firstname = $request->firstname;
        $lastname = $request->lastname;
        $gender = $request->gender;
        $nationality = $request->nationality;
        $bio = $request->message;
        return view("welcome", compact("firstname", "lastname"));
    }
}
