<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    // return form.blade.php
    public function form() {
        return view("form");
    }
}
